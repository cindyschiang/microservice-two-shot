from django.contrib import admin
from hats_rest.models import Hat
from hats_rest.models import LocationVO

# Register your models here.

admin.site.register(Hat)
admin.site.register(LocationVO)
